$(document).foundation();

$("[data-change-movie]").click(function() {
	var id = $(this).data('id');
	$.get('/movie/genre/' + id, {}, function(view) {
		if ( view ) {
			$("#random-movie").html( view );

			$("#genres").foundation('close');
		}
	});
});