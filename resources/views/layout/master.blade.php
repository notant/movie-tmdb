<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

	<link rel="stylesheet" href="/foundation/css/foundation.min.css">
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/ionicons.min.css">

	<style>

	</style>

</head>
<body>


<button type="button" class="canvas-open button" data-toggle="genres">Жанры</button>

<div class="full reveal genres-reveal" id="genres" data-reveal>
	<button class="close-button" data-close aria-label="Close reveal" type="button">
		<span aria-hidden="true">&times;</span>
	</button>
	<div class="menu-container">
		<h2>Жанры</h2>
		<div class="menu">
			@foreach($genres->chunk(10) as $chunk)
				<ul class="menu">
					@foreach($chunk as $genre)
						<li><a data-change-movie data-id="{{$genre->id}}">{{ $genre->title }}</a></li>
					@endforeach
				</ul>
			@endforeach
		</div>
	</div>
</div>

<section id="random-movie">
	@yield('content')
</section>

<section class="seo">
	<h5>Всякий текст для продвижения. Можете смело его игнорировать</h5>
</section>

<!-- Compressed JavaScript -->
<script src="/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="/foundation/js/vendor/foundation.js"></script>
<script src="/foundation/js/app.js"></script>
</body>
</html>