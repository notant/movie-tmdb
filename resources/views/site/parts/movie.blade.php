<div class="small-9 small-centered">
	<div class="media-object stack-for-small">
		<div class="media-object-section middle">
			<div class="thumbnail">
				<img src="{{ \App\Libraries\ImageGrabber::getPosterImage( $movie->poster, 154 ) }}" />
			</div>
		</div>
		<div class="media-object-section main-section middle">
			<h1>{{ $movie->title_rus }}</h1>
			<h3 class="subheader">{{ $movie->title_orig }}</h3>
			<hr>
			<p class="lead">{{ $movie->released_at }}, {{ $movie->tmdb_rating }}</p>
		</div>
	</div>
</div>