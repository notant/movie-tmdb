<?php

Route::get('/', 'SiteController@getIndex');

Route::get('/movie/genre/{id}', 'SiteController@getRandom');


/**
 *
 *
 * movies table: id, tmdb_id, query (range a-z0-9), tmdb_rating, rus_title, original_title; hasOne videos(), hasOne images(), hasMany genres()
 * videos table: id, movie_id, trailer, video_link; hasOne movie()
 * ratings table: id, movie_id, user_id, value (1-10);
 *
 * genres: id, tmdb_id, title;
 * movie_genres: id, movie_id, genre_id;
 *
 * to grab some movies:
 * total_results, total_pages;
 *
 *
 *
 *
 */