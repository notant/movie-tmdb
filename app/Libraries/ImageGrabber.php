<?php


namespace App\Libraries;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Tmdb\Laravel\Facades\Tmdb;

class ImageGrabber {

	protected static $config = [];
	protected static $sizes = [];

	public static function getConfiguration() {
		$cache = Carbon::now()->addYear(1);

		static::$config = Cache::remember('configuration', $cache, function() {
			return Tmdb::getConfigurationApi()->getConfiguration();
		});

		$sizes = [];

		foreach( static::$config['images'] as $key => $config ) {
			if ( str_contains($key, 'sizes') ) {
				$size_key = str_replace( '_sizes', '', $key );

				$sizes[ $size_key ] = [];
				foreach($config as $size) {
					$value = ($size == 'original') ? 1000 : str_replace('w', '', $size);

					$sizes[ $size_key ][ $value ] = $size;
				}
			}
		}

		static::$sizes = $sizes;
	}

	public static function getPosterImage( $path, $width = 500 ) {
		static::getConfiguration();

		$url = static::$config['images']['base_url'];
		$size = static::$sizes['poster'][ $width ];

		return $url . $size . $path;
	}

}

/**
 * [backdrop] => Array ( [300] => w300 [780] => w780 [1280] => w1280 [1000] => original )
 * [logo] => Array ( [45] => w45 [92] => w92 [154] => w154 [185] => w185 [300] => w300 [500] => w500 [1000] => original )
 * [poster] => Array ( [92] => w92 [154] => w154 [185] => w185 [342] => w342 [500] => w500 [780] => w780 [1000] => original )
 * [profile] => Array ( [45] => w45 [185] => w185 [h632] => h632 [1000] => original )
 * [still] => Array ( [92] => w92 [185] => w185 [300] => w300 [1000] => original ) )
 */