<?php

namespace App\Console\Commands;

use App\Genres;
use Illuminate\Console\Command;
use Tmdb\Laravel\Facades\Tmdb;

class CollectGenres extends Command {

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'collect:genres';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Collect genres from TMDB';

	public function handle() {
		$result = Tmdb::getGenresApi()->getGenres([ 'language' => 'ru' ]);

		foreach($result['genres'] as $genre) {
			Genres::firstOrCreate([
				'tmdb_id' => $genre['id'],
				'title' => $genre['name']
			]);
		}
	}

}