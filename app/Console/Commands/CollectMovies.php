<?php

namespace App\Console\Commands;

use App\Genres;
use App\Movies;
use App\MoviesGenres;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Ixudra\Curl\Facades\Curl;
use Mockery\CountValidator\Exception;
use Tmdb\Laravel\Facades\Tmdb;
use Tmdb\Model\Genre;

class CollectMovies extends Command {

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'collect:movies';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Remove duplicate results from DB';

	protected $pages_limit = 1;

	protected $item_per_page = 20;

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$alphabet = range( 'A', 'Z' );
		$numbers  = range( 0, 9 );
		$range    = array_merge( $alphabet, $numbers );

		foreach ( $range as $item ) {
			$info = Tmdb::getSearchApi()->searchMovies( $item, [ 'language' => 'ru', 'page' => 1 ] );

			$count = \App\Movies::where(['query' => $item])->count();
			$last_page = $count / $this->item_per_page;

			$pages_to_grab = $this->pages_limit;

			if ( $count < $info[ 'total_results' ] ) {
				$diff = $info[ 'total_pages' ] - $last_page;
				if ( $diff < $pages_to_grab ) {
					$pages_to_grab = $diff;
				}

				$this->getMovies( $pages_to_grab, $item, $last_page );
			}
		}

	}

	protected function getMovies( $limit, $query, $page = 0 ) {
		$count = 0;

		$movies = $this->getPage( $query, 1 );

//		while ( $count < $limit ) {
//			$count++;
//			$page++;
//
//
//		}
	}

	protected function getPage( $query, $page ) {
		$result = Tmdb::getSearchApi()->searchMovies( $query, [ 'language' => 'ru', 'page' => $page ] );

		$movies = $result['results'];

		foreach($movies as $movie) {
			if ( !$movie['overview'] ) continue;

			$date = explode('-', $movie['release_date']);
			$carbon_date = Carbon::create( $date[0], $date[1], $date[2] );

			$db_mov = Movies::firstOrCreate([
				'tmdb_id' => $movie['id']
			]);
			$db_mov->query = $query;
			$db_mov->title_rus = $movie['title'];
			$db_mov->title_orig = $movie['original_title'];
			$db_mov->tmdb_rating = $movie['vote_average'];
			$db_mov->poster = $movie['poster_path'];
			$db_mov->released_at = $carbon_date;
			$db_mov->save();

			$genres = Genres::whereIn('tmdb_id', $movie['genre_ids'])->get();
			foreach($genres as $genre) {
				MoviesGenres::firstOrCreate([
					'movie_id' => $db_mov->id,
					'genre_id' => $genre->id
				]);
			}

			break;
		}

		return $result;
	}

}