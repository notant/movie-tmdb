<?php

namespace App\Http\Controllers;

use App\Genres;
use App\Libraries\ImageGrabber;
use App\Movies;
use App\MoviesGenres;
use Illuminate\Support\Facades\Request;

class SiteController extends Controller {

	public function getIndex() {
		$movie = Movies::inRandomOrder()->first();

		return view('site.index', [
			'movie' => $movie
		]);
	}

	public function getRandom($id) {
		$moviesIds = MoviesGenres::select('movie_id')->where(['genre_id' => $id])
			->get()
			->pluck('movie_id')
			->toArray();

		if ( !$moviesIds ) return NULL;

		$movie = Movies::inRandomOrder()->whereIn('id', $moviesIds)->first();

		return view('site.parts.movie', [
			'movie' => $movie
		]);
	}

}