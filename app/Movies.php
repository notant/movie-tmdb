<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movies extends Model
{
    protected $fillable = [
    	'tmdb_id',
	    'query',
		'tmdb_rating',
	    'title_rus',
	    'title_orig',
    ];
}
