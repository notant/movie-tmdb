<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('tmdb_id');
	        $table->float('tmdb_rating')->nullable();
	        $table->string('title_rus')->nullable();
	        $table->string('title_orig')->nullable();
	        $table->string('query')->nullable();
	        $table->string('poster')->nullable();
	        $table->date('released_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
